package lab07;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.Assert.*;

public class VeganSandwichTest {
    @Test
    public void veganSandwichConstructTest() {
        VeganSandwich mySandwich = new TofuSandwich();
        assertTrue(mySandwich instanceof VeganSandwich);
    }

    @Test
    public void testAddAndGetFilling() {
        VeganSandwich mySandwich = new TofuSandwich();
        assertEquals("Check if filling is empty", "Tofu", mySandwich.getFilling());
        mySandwich.addFilling("tomato");
        assertEquals("Check if filling is tomato", "Tofu, tomato", mySandwich.getFilling());
    }

    @Test
    public void isVegetarianTest() {
        VeganSandwich mySandwich = new TofuSandwich();
        assertTrue(mySandwich.isVegetarian());
    }

    @Test
    public void isVeganTest() {
        VeganSandwich mySandwich = new TofuSandwich();
        mySandwich.addFilling("tomato, apple, honey");
        assertTrue(mySandwich.isVegan());
    }
}
