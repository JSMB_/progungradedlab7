package lab07;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BagelSandwichTest {
    @Test
    public void testConstructourAndGetters(){
    BagelSandwich bagel = new BagelSandwich();
    assertEquals("constructour should assign leaf as the filling", "", bagel.getFilling());
    }
    @Test
    public void testAddFilling(){
        BagelSandwich bagel = new BagelSandwich();
        bagel.addFilling("leaf, apple");
        assertEquals( "leaf, apple", bagel.getFilling());
    }
    
}
