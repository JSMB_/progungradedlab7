package lab07;

public class TofuSandwich extends VeganSandwich{

    /**
     * Initialises the tofu object and filling to tofu
     */
    public TofuSandwich(){
        super();
        super.addFilling("Tofu");
    }

    /**
     * Overrides the origin getProtein abstract method to return string
     * @return String saying tofu
     */
    @Override
    public String getProtein(){
        return "Tofu";
    }
}
