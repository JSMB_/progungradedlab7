package lab07;

public class CaesarSandwich extends VeganSandwich {

    public CaesarSandwich() {
        super.addFilling("Caeser dressing");
    }

    @Override
    public String getProtein() {
        return "Anchovies";
    }

    @Override
    public boolean isVegetarian() {
        return false;
    }
}
