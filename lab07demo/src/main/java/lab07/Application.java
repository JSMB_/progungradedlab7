package lab07;

public class Application {
    public static void main(String[] args) {
        //1
        VeganSandwich a = new TofuSandwich();

        //2
        ISandwich e = new TofuSandwich();

        //3
        TofuSandwich i = new TofuSandwich();
        if(i instanceof ISandwich) {
            System.out.println(true);
        }

        //4
        if(i instanceof VeganSandwich) {
            System.out.println(true);
        }

        //5
        //i.addFilling("chicken");

        //6
        System.out.println(i.isVegan());

        //7
        ISandwich casted = (VeganSandwich) e;

        //8
        //System.out.println(casted.isVegan());

        //9
        VeganSandwich cObj = new CaesarSandwich();
        System.out.println(cObj.isVegetarian());
    }
}
