package lab07;

public class BagelSandwich implements ISandwich {
    /**
     * Specifies the sandwich's filling
     */
    private String filling;
    /**
     * Confirms if the sandwich is vegetarian or not
     * true = vegetarian
     * false = not vegetarian
     */
    private boolean vegetarian;


    /**
     * Initializes the bagel sandwich object
     * @param filling specifies the sandiwich's filling
     * @param vegetarian specifies whether the sandwich is vegetarian or not
     */
    public BagelSandwich() {
        this.filling = "";
    }

    /**
     * Retrieves a sandwich's filling
     * @return String indicating the sandwich's filling
     */
    public String getFilling() {
        return this.filling;
    }

    /**
     * Assigns a filling to a sandwich object
     * @param filling String to be assigned as the sandwich's new filling
     */
    public void addFilling(String filling) {
        if(this.filling.equals("")) {
            this.filling = filling;
        } else {
            this.filling.concat(", " + filling);
        }
    }

    /**
     * Indicates whether a sandwich object is vegetaran or not
     * @return returns the boolean indicating whether the sandwich is vegetarian or not
     */
    public boolean isVegetarian() {
        throw new UnsupportedOperationException("Not implemented yet");
    }
}
