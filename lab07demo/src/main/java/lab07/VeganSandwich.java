package lab07;
public abstract class VeganSandwich implements ISandwich {
    

    private String filling;


    /**
     * Constructor for the object
     */
    public VeganSandwich(){
        this.filling="";
    }

    /**
     * 
     * @return A string saying the quantity of protein this item has
     */
    public abstract String getProtein();

    /**
     * @Return filling of the object as string
     */

    public String getFilling(){
        return this.filling;
    }



    /**
     * @param topping topping to add as a string
     * Adds filling to the field filling
     */
    public void addFilling(String topping){
        
         String[] meats = {"chicken", "beef", "fish", "meat","pork"};
         for(String meat : meats){
         if(topping.equalsIgnoreCase(meat)){
         throw new IllegalArgumentException("No meat in the vegatarian sandwich");
             }
         }
         if(this.filling.equals("")){
         this.filling = topping;
         }else{
         this.filling += ", " + topping;
         }
    }

    /**
     * @Return boolean determining wether the object has meat or not
     */
    public /*final*/ boolean isVegetarian(){
       return true;
    }


    /**
     * @return boolean determining wether the object has animal products or not
     */ 
    public boolean isVegan(){
        
        String[] splitFilling = this.filling.split(", ", 100);
        String[] nonVegans = {"cheese", "egg"};

        for(String filling : splitFilling){
            for(String nonVegan : nonVegans){
                if(filling.equalsIgnoreCase(nonVegan)){
                    return false;
                }
            }
        }
        return true;
        
    }
}
